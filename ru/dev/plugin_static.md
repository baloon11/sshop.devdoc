# Статика в плагинах

Если для плагина нужно установить свою статику (js, css), то необходимо в настройках проекта добавить статические файли в переменную `EXTRA_JS` и `EXTRA_CSS`. Пример `local_settings.py`:

    EXTRA_JS = [
        'js/bootstrap-popover.js',
        'js/bootstrap-tooltip.js',
        'sco/js/sco.modal.js',
        'js/sshop-informer.js',
    ] + EXTRA_JS

    EXTRA_CSS = [
        'sco/css/scojs.css',
    ] + EXTRA_CSS

Для стилей и скриптов созданы два отдельных шаблона - `extra_css.html` и `extra_js.html` соответствено, которые подключаются в `base.html` (они находятся вне блока `compress`).

При написании плагина со своей статикой, программисту необходимо при установке модифицировать настройки проекта, а именно описанные выше переменные.
