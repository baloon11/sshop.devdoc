# Экспорт даных для навигаторов цен

    class PriceNavigator(models.Model):
        UPDATE_RATES = (
            (0, _(u'Once a day')),
            (5, _(u'Twice a day')),
            (10, _(u'Three times a day')),
            (15, _(u'By request')),
            )

        CODINGS = (
            (0, _(u'utf-8')),
            (5, _(u'windows-1251')),
            )

        active = models.BooleanField(_(u'Active'), default=False)
        name = models.CharField(_(u'Name'), max_length=100)
        file_name = models.CharField(_(u'File name'), max_length=100)
        update_rate = models.IntegerField(_(u'Update rate'), default=0, choices=UPDATE_RATES)
        coding = models.IntegerField(_(u'Coding'), default=0, choices=CODINGS)
        product_statuses = models.ManyToManyField(ProductStatus, verbose_name=_(u'Statuses') )
        pack_to_zip = models.BooleanField(_(u'Pack to zip'), default=False)
        categories = models.ManyToManyField(Category, verbose_name=_(u'Categories'))
        template = models.TextField(_(u'Template'))
        order = models.IntegerField(default=10)
        last_generation = models.DateTimeField(_(u"Last generation"), auto_now_add=True, default=datetime.datetime.now)

* `active` - генерировать ли навигаторы цен
* `name` - название навигатора цен
* `file_name` - название файла с которым будет сохранятся результат
* `update_rate` - частота обновления (генерации)
* `coding` - в какой кодировке нужно сохранять файл
* `product_statuses` - выбор статусов товаров для выгрузки 
* `pack_to_zip` - паковать в zip
* `categories` - выбор категорий для выгрузки
* `template` - шаблон для выгрузки
* `last_generation` - время когда совершена генерация (если генераций еше не было, тогда время создания навигатора)

### Пример шаблона для Hotline.ua

    <price>
        <date>{{ date }}</date>
        <firmName>{{ SHOP.name }}</firmName>
        <firmId>1234</firmId>
        <rate></rate>
        <categories>
            {% for category in categories %}
            <category>
                <id>{{ category.id }}</id>
                <name>{{ category.name }}</name>
            </category>
            {% endfor %}
        </categories>
        <items>
            {% for product in products %}
            <item>
                <id>{{ product.id }}</id>
                <categoryId>{{ product.get_category.id }}</categoryId>
                <code>{{ product.sku }}</code>
                <vendor>{{ product.manufacturer.name }}</vendor>
                <name>{{ product.name }}</name>
                <description>{{ product.short_description }}</description>
                <url>{{ site }}{{ product.get_absolute_url }}</url>
                <image>{{ product.get_image.image.url }}</image>
                <priceRUAH>{{ product.price }}</priceRUAH>
                {% if product.status.show_buy_button %}
                <stock>На складе</stock>
                {% endif %}
                <guarantee>{{ product.guarantee }}</guarantee>
            </item>
            {% endfor %}
        </items>
    </price>