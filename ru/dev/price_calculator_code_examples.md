# Примеры кода

### Реализация `RegionPriceCalculator`

    class RegionPriceCalculator(PriceCalculator):

        def get_price_coefficient(self):
            if 'region' in self.request.session:
                return Region.objects.get(pk=self.request.session.get('region')).coefficient
            else:
                return Region.objects.get(is_default=True).coefficient
       
        def get_price(self, with_properties=True):
            coefficient = self.get_price_coefficient()
            return super(RegionPriceCalculator, self).get_price(with_properties) * coefficient

        def get_standard_price(self, with_properties=True):
            coefficient = self.get_price_coefficient()
            return super(RegionPriceCalculator, self).get_standard_price(with_properties) * coefficient

        def get_for_sale_price(self, with_properties=True):
            coefficient = self.get_price_coefficient()
            return super(RegionPriceCalculator, self).get_for_sale_price(with_properties) * coefficient

В методе `get_price_coefficient` мы читаем из сессии значение `region` и возвращаем его коэффициент. Если в сессии не записан регион, то возвращаем коэффициент региона с `is_default=True`.  
Далее переопределяем все методы получения цены, добавляя в них умножение цены этого же метода родительского класса на найденный коэффиент.

Кроме self каждый метод имеет аргумент `with_properties` - если `True`, то для товаров с вариантами конечная цена будет рассчитана с учетом дефолтного варианта.

### Реализация `RegionShippingPriceCalculator`

    class RegionShippingPriceCalculator(ShippingMethodPriceCalculator):
       
        def get_price_coefficient(self):
            if 'region' in self.request.session:
                return Region.objects.get(pk=self.request.session.get('region')).coefficient
            else:
                return Region.objects.get(is_default=True).coefficient

        def get_price(self):
            coefficient = self.get_price_coefficient()
            return super(RegionShippingPriceCalculator, self).get_price() * coefficient

Тут все аналогично.

