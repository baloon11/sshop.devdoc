# Инициализация БД

Для правильной работы магазина необходимо провести инициализацию БД (после создания БД), для этого нужно запустить команду `./manage.py lfs_init`. Которая создаст стандартные данные:

* `usa = Country.objects.create(code="us", name="USA")`
* `dollar = Currency.objects.create(name="Dollar", code="usd", abbr="USD")` - валюта магазина
* `shop = Shop.objects.create(name="7shop", ..., default_country=usa, default_currency=dollar)` - магазин с именем 7shop, страной США и валютой доллар.
* `Slot.objects.create(name="Left")` - левый слот для портлетов
* `Slot.objects.create(name="Right")` - правый слот для портлетов  
* `Slot.objects.create(name="Middle", type_of_slot=5)` - верхний(горизонтальный) слот для портлетов
* `categories_portlet = CategoriesPortlet.objects.create(title="Categories")` - создание портлета категории
* `PortletAssignment.objects.create(slot=left_slot, content=shop, portlet=categories_portlet)` - назначение портлета категории в левый слот портлетов.
* `PaymentMethod.objects.create(name="Cash on delivery", currency=dollar, priority=1, active=1, deletable=0)` - создание метода оплаты 
* `ShippingMethod.objects.create(name="Standard", priority=1, active=1)` - создание метода доставки.
* `ProductStatus.objects.create(name=u"В наличии", css_class='label-success')` - создание статуса товара "В наличии"
* `ProductStatus.objects.create(name=u"Нет в наличии", show_buy_button=False)` - создание статуса товара "Нет в наличии" и невозможностью купить товары с даным статусом
* `SortType.objects.create(name=u'По возрастанию цены', sortable_fields='price')` - создание сортировки по полю `price`