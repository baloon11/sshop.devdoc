# Расчет цены доставки

В базовой поставке Aquashop присутствуют несколько готовых калькуляторов цен доставки.

## class DefaultShippingPriceCalculator(ShippingMethodPriceCalculator)

Калькулятор цен доставки по умолчанию, полностью наследует класс `ShippingMethodPriceCalculator` не добавляя ничего нового. Возвращает цену указанную для метода доставки не изменяя её.

## class RegionShippingPriceCalculator(ShippingMethodPriceCalculator)

Возвращает цену указанную для метода доставки умноженную на коэффициент региона.

При значении `ALLOW_REGIONS = True` добавляется следующая настройка, которая и включает калькулятор цен доставки.

    LFS_SHIPPING_METHOD_PRICE_CALCULATORS += [
        ['regions.RegionShippingPriceCalculator', ugettext(u'Regions shipping price calculator')],
    ]
