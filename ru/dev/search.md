# Поиск

Приложение поиска находится в папке `sshop/lfs/search/` и подключается в `INSTALLED_APPS` файла `settings.py`.

Работа поиска устроена довольно просто. В файла `views.py` имеется два представления: `search` и `livesearch`. Первое обрабатывает поисковый запрос и перенаправляет пользователя на страницу с результатом, второе - вызывается ajax-запросом и результат отображается при "быстром" поиске (количество выводимых элементов ограничено в коде - 5 шт).

Поиск может работать в двух режимах - используя стандартный ORM Django и средства PostgreSQL. В штатном режиме используется ORM. Для включение второго режимы, в `settings.py` переменной `ENABLE_FULL_TEXT_SEARCH` нужно присвоить значение `True`.

#### ORM Django

Поиск осуществляется с помощью объекта `Q` по полям `name`, `manufacturer`, `sku_manufacturer`, `sub_type` объекта класса `Product`. В коде это имеет следующий вид:

    query = Q(name__icontains=q) | \
            Q(manufacturer__name__icontains=q) | \
            Q(sku_manufacturer__icontains=q) & \
            Q(sub_type__in=(STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS, VARIANT))

    products = Product.objects.filter(query).filter(active=True)

#### PostgreSQL

Поиск выполняется внутренними механизмами PostgreSQL. Для создание полнотекстового поиска по таблице нужно создать пустую миграцию в которой пишется SQL, который вносит специальные изменения в таблицу.

В данный момент это миграция `0011_fulltext_search_migration.py`. Сейчас команды выглядят так:

    ALTER TABLE catalog_product ADD COLUMN product_tsv tsvector;
    CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE ON catalog_product 
        FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger(product_tsv, 'pg_catalog.russian', name, sku_manufacturer, description);
    CREATE INDEX catalog_product_tsv ON catalog_product USING gin(product_tsv);
    UPDATE catalog_product SET product_tsv=to_tsvector(name) || \
                                           to_tsvector(sku_manufacturer) || \
                                           to_tsvector(description);


Эти команды создают специальное поле `product_tsv` для индексации в таблице `catalog_product`. При этом индексируются такие поля таблицы: `name`, `sku_manufacturer`, `description`.

В Django-приложениях используется следующим образом:

    products = Product.objects.extra(
        where=['product_tsv @@ plainto_tsquery(%s)'],
        params=[q]
        ).filter(active=True)

Где `q` - слово/фраза для поиска.

**Также написана функция для отката данной миграции. Было проведено несколько тестов и все нормально отработало, НО откатом данной миграции надо пользоваться очень осторожно, возможно некоторые моменты не учтены и это может привести к потери данных!!!**

#### Ссылки

Данная реализация сделана на основе [статья 1](http://www.ironzebra.com/news/26/how-to-add-full-text-search-to-your-django-app-with-postgresql-and-south-migrations), [статья 2](http://barryp.org/blog/entries/postgresql-full-text-search-django/).
