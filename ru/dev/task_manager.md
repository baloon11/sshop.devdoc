# Диспетчер задач

За взаимодействие с диспетчером задач в движке магазина отвечает аппликация `tasks`.

Для управления диспетчером задач используется модель `tasks.models.Task`:

    class Task(models.Model):
        TASK_STATUSES = (
            (0, _(u'Pending')),
            (5, _(u'Success')),
            (10, _(u'Failed')),
            )

        function_name = models.CharField(max_length=200, default='')
        arguments = models.TextField(default='{}')
        added_at = models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)
        finished_at = models.DateTimeField(default=datetime.datetime.now)
        status = models.IntegerField(default=0, choices=TASK_STATUSES)
        error_msg = models.TextField(default='', blank=True)
        run_after = models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)

Здесь:

* `function_name` - функция которую нужно запустить;
* `arguments` - аргументы которые нужно передать функции;
* `added_at` - дата и время добавления нового задания;
* `finished_at` - дата и время окончания задания;
* `status` - текущий статус задачи;
* `error_msg` - текст сообщение об ошибке, если задача закончилась неудачей.
* `run_after` - после какого времени нужно запускать задание

## Добавление новой задачи

Для добавление новой задачи используется функция `tasks.utils.add_task` получает на вход три параметра `function_name`, `arguments` и `run_after`:

* `function_name` - функция которую нужно запустить (пример `lfs.filters.utils.update_filters`);
* `arguments` - аргументы с которыми нужно запустить функцию (пример `{'category_id':15}`).
* `run_after` - время после которого запускать задание (по умолчанию datetime.now()).

Если нет задачи с такими параметрами и статусом `Pending`, то создается новая задача.

## Запуск задач

За запуск задач отвечает команда `./manage.py run_tasks`. 

Каждая задача со статусом `Pending` отправляется на обработку, если задача завершилась успешно, то статус меняется на `Success`, иначе сообщение об ошибке записывается в `error_msg` и статус меняется на `Failed`.
