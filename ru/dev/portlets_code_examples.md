# Примеры кода

### Реализация `EmptySpacePortlet`

    class EmptySpacePortlet(Portlet):
        """Portlet to display empty space.
        """

        size = models.TextField(_(u"Empty size(px)"), blank=True)

        class Meta:
            app_label = 'portlet'

        def __unicode__(self):
            return "%s" % self.id

        def render(self, context):
            request = context.get("request")
            try:
                size = int(filter(lambda x:x.isdigit(), self.size))
            except:
                size = 0
            return render_to_string("lfs/portlets/empty_space.html", RequestContext(request, {
                'size': size,
                'slot_name': context.get('slot_name'),
                'main_page': context.get('main_page'),
            }))


        def form(self, **kwargs):
            return EmptySpaceForm(instance=self, **kwargs)


    class EmptySpaceForm(forms.ModelForm):
        """Form for the CarouselPortlet.
        """
        class Meta:
            model = EmptySpacePortlet

В поле `size` указывается размер в пикселях, скоко пустого пространства должен портлет визуально освободить.
Метод `render` - обязательный для каждого портлета, этот метод вызывается перед отображением портлета, и возвращает хтмл который нужно вставить на страницу.